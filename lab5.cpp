/*******************   
your name: Elijah Webb   
username: egwebb                
Lab 5                               
Lab Section:004   
Name of TA: Anurata Prabha Hridi              
*******************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
	Card deck[52];
	int k = 0;
	int i;
		//This loop iterates through each suit and all values to create the deck
		for(i = 0;i < 4;i++){
			for(int j = 2;j < 15;j++){
				deck[k].suit = static_cast<Suit>(i);
				deck[k].value = j;
				k++;
			}
		}
  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
		random_shuffle(deck,&deck[52],myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
		Card hand[5] = {deck[0],deck[1],deck[2],deck[3],deck[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
		sort(hand, &hand[5], suit_order);
    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
		//I use setw to make sure the hand lines up when printed
		for(int l = 0;l < 5;l++){
			cout << setw(10) << get_card_name(hand[l]) << 
			get_suit_code(hand[l]) << endl;
		}

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  //compares suit
	if(lhs.suit < rhs.suit){
		return true;	
	}
	//if suit is the same then it compares the value
	if(lhs.suit == rhs.suit && lhs.value < rhs.value){
		return true;
	}
	//if it is in the wrong order it returns false
	else{
		return false;
	}	
	
	
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:	return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
	//the card's value is the case statement
	switch(c.value){
		case 2:	 return "2 of ";
		case 3:	 return "3 of ";
		case 4:  return "4 of ";
		case 5:  return "5 of ";
		case 6:  return "6 of ";
		case 7:  return "7 of ";
		case 8:  return "8 of ";
		case 9:	 return "9 of ";
		case 10: return "10 of ";
		case 11: return "Jack of ";
		case 12: return "King of ";
		case 13: return "Queen of ";
		case 14: return "Ace of ";
		default: return "";
	}		
}








